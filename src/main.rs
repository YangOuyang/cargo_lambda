use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};

/// receiving an array of number and compute their mean, median, mode, max, min, and standard deviation
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract list of numbers from the request query string, defaulting to "[1,3,5,6]" if not provided.
    let query_param_numbers = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("numbers"))
        .unwrap_or("1,3,5,6");

// Parse the query parameter string into a vector of integers.
    let numbers_result: Result<Vec<i32>, _> = query_param_numbers.split(",").map(str::parse).collect();

    println!("numbers_result: {:?}", numbers_result);
// Proceed only if parsing is successful.
    if let Ok(numbers) = numbers_result {
        // Calculate mean
        let mean = numbers.iter().sum::<i32>() as f64 / numbers.len() as f64;

        // Calculate median
        let median = {
            let mut sorted_numbers = numbers.clone();
            sorted_numbers.sort_unstable();
            let mid_index = sorted_numbers.len() / 2;
            if sorted_numbers.len() % 2 == 0 {
                (sorted_numbers[mid_index - 1] + sorted_numbers[mid_index]) as f64 / 2.0
            } else {
                sorted_numbers[mid_index] as f64
            }
        };

        // Calculate mode
        let mode = {
            let mut frequency_map = std::collections::HashMap::new();
            for &number in &numbers {
                *frequency_map.entry(number).or_insert(0) += 1;
            }
            frequency_map
                .into_iter()
                .max_by_key(|&(_, count)| count)
                .map(|(val, _)| val)
                .unwrap() // Assuming there is at least one number, thus at least one mode.
        };

        // Find max and min
        let max = *numbers.iter().max().unwrap();
        let min = *numbers.iter().min().unwrap();

        // Calculate standard deviation
        let std_deviation = {
            let variance = numbers
                .iter()
                .map(|&x| {
                    let diff = x as f64 - mean;
                    diff * diff
                })
                .sum::<f64>()
                / numbers.len() as f64;
            variance.sqrt()
        };

        // Debug or use the calculated statistics as needed
        println!("Mean: {}, Median: {}, Mode: {}, Max: {}, Min: {}, Standard Deviation: {}", mean, median, mode, max, min, std_deviation);

        let message = format!(
            "<!DOCTYPE html>
    <html lang=\"en\">
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <title>Statistics Overview</title>
        <style>
            body {{
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100vh;
                display: flex;
                justify-content: center;
                align-items: center;
                font-family: 'Orbitron', sans-serif;
                background-color: #000;
                color: #0f0;
            }}

            .white-text {{
                color: #fff;
            }}
            .title {{
                color: #ff2052;
                margin-bottom: 20px;
                font-size: 24px;
            }}
            .statistic {{
                margin-top: 10px;
                font-size: 20px;
                padding-bottom: 5px;
            }}
            .statistic-key {{
                font-weight: bold;
                display: inline;
            }}
            .statistic-value {{
                color: #ff2052; /* Neon pink to differentiate the value */
                display: inline;
            }}
            .statistic:not(:last-child) {{
                border-bottom: 1px solid #0f0; /* Neon green underline for separation */
                padding-bottom: 10px;
                margin-bottom: 10px;
            }}
            @import url('https://fonts.googleapis.com/css2?family=Orbitron&display=swap');
        </style>
    </head>
    <body>
        <div class=\"container\">
            <div class=\"title\">Author: <span class=\"white-text\"> Yang Ouyang</span></div>
            <div class=\"title\">Functionality: <span class=\"white-text\"> Statistics Overview</span> </div>
            <div class=\"statistic\"><span class=\"statistic-key\">Number List:</span> <span class=\"statistic-value\">{query_param_numbers}</span></div>
            <div class=\"statistic\"><span class=\"statistic-key\">Mean:</span> <span class=\"statistic-value\">{mean}</span></div>
            <div class=\"statistic\"><span class=\"statistic-key\">Median:</span> <span class=\"statistic-value\">{median}</span></div>
            <div class=\"statistic\"><span class=\"statistic-key\">Mode:</span> <span class=\"statistic-value\">{mode}</span></div>
            <div class=\"statistic\"><span class=\"statistic-key\">Max:</span> <span class=\"statistic-value\">{max}</span></div>
            <div class=\"statistic\"><span class=\"statistic-key\">Min:</span> <span class=\"statistic-value\">{min}</span></div>
            <div class=\"statistic\"><span class=\"statistic-key\">Standard Deviation:</span> <span class=\"statistic-value\">{std_deviation}</span></div>
            <div class=\"statistic\"></div>
        </div>
    </body>
    </html>",
            query_param_numbers = query_param_numbers,
            mean = mean,
            median = median,
            mode = mode,
            max = max,
            min = min,
            std_deviation = std_deviation
        );


        let resp = Response::builder()
            .status(200)
            .header("content-type", "text/html")
            .body(message.into())
            .map_err(Box::new)?;
        Ok(resp)
    } else {
        // Handle parsing error, such as logging or returning an error response
        eprintln!("Failed to parse the numbers from the query parameter.");
        let resp = Response::builder()
            .status(400)
            .body("Failed to parse the numbers from the query parameter.".into())
            .map_err(Box::new)?;
        Ok(resp)
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
