watch:
	cargo lambda watch

build:
	cargo lambda build --release --arm64

deploy:
	cargo lambda deploy --region us-east-1
